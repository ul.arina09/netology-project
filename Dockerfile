FROM centos:7

RUN yum -y groupinstall "Development Tools" \
    && yum -y install openssl-devel bzip2-devel libffi-devel wget python3 \
    && wget https://www.python.org/ftp/python/3.8.7/Python-3.8.7.tgz \
    && tar xvf Python-3.8.7.tgz \
    && cd Python-3.8.7 \
    && ./configure --enable-optimizations \
    && make altinstall \
    && rm -rf ../Python-3.8.7 ../Python-3.8.7.tgz \
    && yum clean all

RUN pip3 install flask flask-restful flask-jsonpify

WORKDIR /python-api
COPY python-api.py /python-api
EXPOSE 5290
CMD python3.8 /python-api/python-api.py
